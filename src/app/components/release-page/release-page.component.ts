import {Component, OnInit, ViewChild} from '@angular/core';
import {Release} from '../../models/release.model';
import {ReleaseService} from '../../services/release.service';
import {PageData} from '../../models/page-data.model';
import {CdkVirtualScrollViewport} from '@angular/cdk/scrolling';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Track} from '../../models/track.model';

@Component({
  selector: 'app-release-page',
  templateUrl: './release-page.component.html',
  styleUrls: ['./release-page.component.scss']
})
export class ReleasePageComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(CdkVirtualScrollViewport) viewport: CdkVirtualScrollViewport;
  public dataSource: MatTableDataSource<Release>;
  displayedColumns = ['title', 'artists', 'created'];
  filter = '';
  scrollIndex = 0;
  releases: Release[];
  pageData: PageData;
  constructor(private releaseService: ReleaseService) { }

  ngOnInit() {
    this.releases = [];
    this.pageData = new PageData();
    this.dataSource = new MatTableDataSource(this.releases);
    this.dataSource.sort = this.sort;
    this.loadReleaseData();
  }
  loadReleaseData(filter = '', sortDirection = 'asc', pageIndex = 0, pageSize = 20, paged = true, append = false) {
    this.releaseService.findAll(filter, sortDirection, pageIndex, pageSize, paged, this.sort).subscribe(res => {
      if (res.page) {
        this.pageData = PageData.deserialize(res.page);
      }
      if (res.length === 0) {
        return;
      }
      this.releases = Release.deserializeArray(res);
      this.dataSource = new MatTableDataSource(this.releases);
    });
  }
  applyFilter(filter?: string) {
    if (filter === undefined) {
      filter = this.filter;
    }
    this.pageData.number = 0;
    this.loadReleaseData();
  }

}
