import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';
import {fillViewport} from '../../utility/functions';
import {CdkVirtualScrollViewport} from '@angular/cdk/scrolling';

@Component({
  selector: 'app-collection-container',
  templateUrl: './collection-container.component.html',
  styleUrls: ['./collection-container.component.scss']
})
export class CollectionContainerComponent implements OnInit, AfterViewInit {
  @ViewChild(CdkVirtualScrollViewport) viewport: CdkVirtualScrollViewport;
  @Output() filter = new EventEmitter<string>();
  @Output() loadMore = new EventEmitter<boolean>();
  @Input() search = true;
  @Input() itemSize = 50;

  constructor(private breakpointObserver: BreakpointObserver) {
  }
  ngOnInit() {}
  onResize() {
    fillViewport('scroll-viewport');
  }
  ngAfterViewInit(): void {
    this.onResize();
  }

  applyFilter(value: string) {
    this.filter.emit(value);
  }
  handleScroll() {
    if (this.viewport.measureScrollOffset('bottom') <= 40) {
      this.loadMore.emit(true);
    }
  }
}

