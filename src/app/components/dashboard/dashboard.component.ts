import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Track} from '../../models/track.model';
import {TrackService} from '../../services/track.service';
import {ArtistService} from '../../services/artist.service';
import {Artist} from '../../models/artist.model';
import {FileService} from '../../services/file.service';
import {ReleaseService} from '../../services/release.service';
import {Release} from '../../models/release.model';
import {PlayerService} from '../../services/player.service';
import {PlaylistService} from '../../services/playlist.service';
import {fillViewport} from '../../utility/functions';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent  implements OnInit {
  tracks: Track[] = [];
  artists: Artist[] = [];
  releases: Release[] = [];
  tracksLoaded: Promise<Boolean>;
  artistsLoaded: Promise<Boolean>;
  releasesLoaded: Promise<Boolean>;
  files: FileList;
  constructor(public trackService: TrackService,
              private fileService: FileService,
              private artistService: ArtistService,
              private releaseService: ReleaseService,
              private playlistService: PlaylistService,
              private playerService: PlayerService) {}


  ngOnInit() {
    this.randomizeTracks();
    this.randomizeArtists();
    this.randomizeRelease();
  }
  randomizeTracks()  {
    this.tracksLoaded = Promise.resolve(false);
    this.trackService.findRandom(5).subscribe(res => {
      this.tracks = res;
      this.tracksLoaded = Promise.resolve(true);
    });
  }
  randomizeArtists() {
    this.artistsLoaded = Promise.resolve(false);
    this.artistService.findRandom(3).subscribe(res => {
      this.artists = res;
      this.artistsLoaded = Promise.resolve(true);
    });
  }
  randomizeRelease() {
    this.releasesLoaded = Promise.resolve(false);
    this.releaseService.findRandom(1).subscribe( res => {
      this.releases = res;
      this.releasesLoaded = Promise.resolve(true);
    });
  }
  playTrack(trackId: string) {
    this.trackService.play(trackId).subscribe(() => {
      this.playerService.findTracks();
    });
  }
}
