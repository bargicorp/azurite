import {AfterViewInit, Component, EventEmitter, Injectable, Input, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {Player} from '../../models/player.model';
import {PlayerService} from '../../services/player.service';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {Track} from '../../models/track.model';
import {map} from 'rxjs/operators';
import {RxStompService} from '@stomp/ng2-stompjs';
import {Playlist} from '../../models/playlist.model';
import {PlaylistService} from '../../services/playlist.service';
import {fillViewport} from '../../utility/functions';
import {MatTabGroup} from '@angular/material';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
})
export class PlayerComponent implements OnInit, AfterViewInit {
  @ViewChild(MatTabGroup) matTabGroup: MatTabGroup;
  playlists: Playlist[];
  player: Player;
  playlistEditorTracks: Track[];
  tracks: Track[];
  playlistEditorTitle = '';

  constructor(private playerService: PlayerService,
              public playlistService: PlaylistService,
              private rxStompService: RxStompService) {
    this.playlists = [];
  }
  ngOnInit() {
    this.loadPlayerData();
    this.loadTrackData();
    this.playlistService.editorTitle.subscribe(title => {
      this.handlePlaylistEditorContentChange(title);
    });
    this.playlistService.editorTrackData.subscribe(tracks => {
      this.playlistEditorTracks = tracks;
    });
    this.rxStompService.watch('/updates/player').subscribe(() => {
      this.loadPlayerData();
    });
    this.rxStompService.watch('/updates/player/tracks').subscribe(() => {
      if (this.playerService.expireWebsocketIgnoreBuffer()) {
        this.loadTrackData();
      }
    });
    this.rxStompService.watch('/updates/playlists').subscribe(playlist => {
      if (playlist.binaryBody && this.playlistService.activeEditorPlaylist) {
        if (new TextDecoder('utf-8').decode(playlist.binaryBody) === this.playlistService.activeEditorPlaylist.id) {
          setTimeout(() => {
            this.playlistService.findEditorTracks(); },  200);
        }
      }
    });
  }
  tabSwitch() {
    setTimeout(() => {
      this.onResize();
      }, 200);
  }
  ngAfterViewInit() {
    this.onResize();
  }
  onResize() {
      if (document.getElementById('player-scroll-viewport')) {
        fillViewport('player-scroll-viewport');
      }
      if (document.getElementById('playlist-editor-scroll-viewport')) {
        fillViewport('playlist-editor-scroll-viewport');
      }
  }
  move(event: CdkDragDrop<string[]>, playlist?: Playlist) {
    if (playlist == null) {
      moveItemInArray(this.tracks, event.previousIndex, event.currentIndex);
      if (event.previousIndex === this.player.index) {
        this.player.index = event.currentIndex;
      } else if (event.previousIndex < this.player.index) {
        if (event.currentIndex >= this.player.index) {
          this.player.index--;
        }
      } else if (event.previousIndex > this.player.index) {
        if (event.currentIndex <= this.player.index) {
          this.player.index++;
        }
      }
      this.playerService.moveTrack(event.previousIndex, event.currentIndex).subscribe(() => {
        this.loadTrackData();
        this.playerService.findPlayer();
      });
    } else {
      moveItemInArray(this.playlistEditorTracks, event.previousIndex, event.currentIndex);
      this.playlistService.moveTrack(playlist, event.previousIndex, event.currentIndex).subscribe();
    }
  }
  loadPlayerData() {
    this.playerService.findPlayer();
    this.playerService.playerData.subscribe((player: Player) => {
      this.player = player;
    });
  }
  loadTrackData() {
    this.playerService.findTracks();
    this.playerService.trackData.subscribe(tracks => {
      this.tracks = [];
      tracks.map((i) => {
        const artists = [];
        if (i._embedded.artists) {
          artists.push(i._embedded.artists);
        } else {
          artists.push(i._embedded.artist);
        }
        const track = new Track(
          i.id,
          i.title,
          i.duration
        );
        track.artists = artists;
        this.tracks.push(track);
      });
    });
  }
  previous() {
    this.playerService.play(this.player.index - 1);
  }
  next() {
    this.playerService.play(this.player.index + 1);
  }
  stop() {
    this.playerService.stop();
  }
  togglePlayPause() {
      this.playerService.togglePause(this.player.paused);
  }
  play(index: number) {
    this.playerService.play(index);
  }
  startPlaylist(playlist: Playlist, index: number = 0) {
    this.playerService.setTracks(Track.getIds(playlist.tracks), index);
    this.playlistService.activeEditorPlaylist = null;
  }
  handlePlaylistEditorContentChange(title: string) {
    if (this.playlistService.activeEditorPlaylist) {
      if (title !== this.playlistEditorTitle) {
        this.playlistEditorTitle = title;
        this.matTabGroup.selectedIndex = 0;
      }
    }
  }
}
