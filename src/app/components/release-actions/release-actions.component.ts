import {Component, Input, OnInit} from '@angular/core';
import {PlayerService} from '../../services/player.service';
import {Release} from 'src/app/models/release.model';
import {Track} from 'src/app/models/track.model';
import {ReleaseService} from '../../services/release.service';

@Component({
  selector: 'app-release-actions',
  templateUrl: './release-actions.component.html',
  styleUrls: ['./release-actions.component.scss']
})
export class ReleaseActionsComponent implements OnInit {
  @Input() release: Release;

  constructor(private playerService: PlayerService, private releaseService: ReleaseService) {
  }

  ngOnInit() {
  }
  private playTracks() {
    this.playerService.setTracks(Track.getIds(this.release.tracks), 0);
  }
  play($event) {
    $event.stopPropagation();
    if (!this.release.tracks) {
      this.releaseService.getTracks(this.release.id).subscribe(res => {
        this.release.tracks = res;
        this.playTracks();
      });
    } else {
      this.playTracks();
    }
  }
}

