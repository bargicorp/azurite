import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';
import {CdkVirtualScrollViewport} from '@angular/cdk/scrolling';
import {Release} from '../../models/release.model';
import {PageData} from '../../models/page-data.model';
import {ReleaseService} from '../../services/release.service';
import {Artist} from '../../models/artist.model';
import {ArtistService} from '../../services/artist.service';
import {fillViewport} from '../../utility/functions';

@Component({
  selector: 'app-artist-page',
  templateUrl: './artist-page.component.html',
  styleUrls: ['./artist-page.component.scss']
})
export class ArtistPageComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(CdkVirtualScrollViewport) viewport: CdkVirtualScrollViewport;
  public dataSource: MatTableDataSource<Artist>;
  displayedColumns = ['name', 'track-count', 'last-modified'];
  filter = '';
  artists: Artist[];
  pageData: PageData;
  constructor(private artistService: ArtistService) { }

  ngOnInit() {
    this.artists = [];
    this.pageData = new PageData();
    this.dataSource = new MatTableDataSource(this.artists);
    this.dataSource.sort = this.sort;
    this.loadArtistData();

  }
  onResize() {
    // fillViewport('');
  }
  loadArtistData(filter = '', sortDirection = 'asc', pageIndex = 0, pageSize = 20, paged = true, append = false) {
    this.artistService.findAll(filter, sortDirection, pageIndex, pageSize, paged, this.sort).subscribe(res => {
      if (res.page) {
        this.pageData = PageData.deserialize(res.page);
      }
      if (res.length === 0) {
        return;
      }
      this.artists = Artist.deserializeArray(res);
      this.dataSource = new MatTableDataSource(this.artists);
    });
  }
  applyFilter(filter?: string) {
    if (filter === undefined) {
      filter = this.filter;
    }
    this.pageData.number = 0;
    this.loadArtistData(filter);
  }

}
