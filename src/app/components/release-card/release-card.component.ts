import {Component, Input, OnInit} from '@angular/core';
import {Release} from '../../models/release.model';
import {ReleaseService} from '../../services/release.service';

@Component({
  selector: 'app-release-card',
  templateUrl: './release-card.component.html',
  styleUrls: ['./release-card.component.scss']
})
export class ReleaseCardComponent implements OnInit {
  @Input() tracksDisabled: boolean;
  @Input() release: Release;
  loaded: Promise<Boolean>;
  extracted = false;
  constructor(private releaseService: ReleaseService) {}

  ngOnInit() {
    this.releaseService.findCover(this.release).subscribe(res => {
      this.release.coverArt = res;
    });
  }
  toggleTracks() {
    if (this.release.tracks === undefined) {
      this.releaseService.getTracks(this.release.id).subscribe(res => {
        this.release.tracks = res;
        this.loaded = Promise.resolve(true);
        this.extracted = true;
      });
    } else {
      this.extracted = !this.extracted;
    }
  }

}
