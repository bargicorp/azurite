import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Release} from '../../models/release.model';
import {ReleaseCardComponent} from '../release-card/release-card.component';
import {ReleaseService} from '../../services/release.service';
import {IdParser} from '../../utility/id-parser';
import {ActivatedRoute} from '@angular/router';
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-release',
  templateUrl: './release.component.html',
  styleUrls: ['./release.component.scss']
})
export class ReleaseComponent implements OnInit, AfterViewInit {
  loaded: Promise<Boolean>;
  release: Release;
  title = 'release not found';
  @ViewChild(ReleaseCardComponent) card: ReleaseCardComponent;
  constructor(private route: ActivatedRoute, private releaseService: ReleaseService) { }

  ngOnInit() {
    this.releaseService.findRelease(IdParser.parseReleaseId(this.route)).subscribe(res => {
      this.release = res;
      this.title = 'Release: ' + res.title;
      this.loaded = Promise.resolve(true);
    });
  }
  ngAfterViewInit() {
    setTimeout (() => {
      this.card.toggleTracks();
    }, 300);
  }

}
