import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {Playlist} from '../../models/playlist.model';
import {PlaylistService} from '../../services/playlist.service';
import {PlayerService} from '../../services/player.service';
import {fillViewport} from '../../utility/functions';
import {RxStompService} from '@stomp/ng2-stompjs';
import {ContentObserver} from '@angular/cdk/observers';

@Component({
  selector: 'app-playlist-page',
  templateUrl: './playlist-page.component.html',
  styleUrls: ['./playlist-page.component.scss']
})
export class PlaylistPageComponent implements OnInit, AfterViewInit {
  playlists: Playlist[] = [];
  nothingToShowText = 'Loading ...';
  selectedPlaylist: Playlist;
  tracklistColumns = ['title', 'artists', 'release'];
  playlistColumns = ['name', 'created-date'];
  edit = false;

  constructor(private playerService: PlayerService, private playlistService: PlaylistService, private rxStompService: RxStompService) {
    this.selectedPlaylist = null;
  }
  ngOnInit() {
    this.load();
    this.rxStompService.watch('/updates/playlists').subscribe(playlist => {
      if (playlist.binaryBody && this.playlistService.activeEditorPlaylist) {
        if (new TextDecoder('utf-8').decode(playlist.binaryBody) === this.selectedPlaylist.id) {
          setTimeout(() => {
            this.playlistService.loadTracks(this.selectedPlaylist).subscribe(res => {
              this.selectedPlaylist = res;
            }); },  200);
        }
      }
    });
  }
  onResize() {
    fillViewport('playlist-scroll-viewport');
    fillViewport('tracklist-scroll-viewport');
  }
  ngAfterViewInit() {
    this.onResize();
  }
  load() {
    this.playlistService.findAll().subscribe(res => {
      this.playlists = res;
      if (res.length === 0) {
        this.nothingToShowText = 'No playlists have been found ...';
      }
    });
  }
  applyFilter(filter: string) {
    // x
  }
  addPlaylist() {
    this.playlistService.create().subscribe(res => {
      this.selectedPlaylist = Playlist.deserialize(res);
      this.load(); });
  }
  selectPlaylist(playlist: Playlist) {
    this.selectedPlaylist = playlist;
    this.edit = false;
    if (!playlist.tracks) {
      this.playlistService.loadTracks(playlist).subscribe(res => {
        playlist = res;
        this.selectedPlaylist = res;
      });
    }
  }
  toggleEdit() {
    if (!this.edit) {
      this.edit = true;
    } else {
      this.save();
    }
  }
  save() {
    this.edit = false;
    this.playlistService.update(this.selectedPlaylist).subscribe();
  }
  delete() {
    this.playlistService.delete(this.selectedPlaylist).subscribe( () => {
      this.selectedPlaylist = null;
      this.load();
      }
    );
  }

}
