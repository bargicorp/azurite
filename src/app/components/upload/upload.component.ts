import { Component, OnInit } from '@angular/core';
import {FileService} from '../../services/file.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

  files: FileList;
  constructor(private fileService: FileService) { }

  ngOnInit() {
  }
  changeFile(event) {
    this.files = event.target.files;
  }
  uploadSelection() {
    for (const item in this.files) {
      if (this.files.hasOwnProperty(item)) {
        this.fileService.upload(this.files[item]);
        this.files = null;
      }
    }
  }
  uploadDrop(files: FileList) {
    for (const item in files) {
      if (files.hasOwnProperty(item)) {
        this.fileService.upload(files[item]);
      }
    }
  }
}
