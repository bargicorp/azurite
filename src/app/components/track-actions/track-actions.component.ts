import {Component, Input, OnInit} from '@angular/core';
import {TrackService} from '../../services/track.service';
import {PlayerService} from '../../services/player.service';
import {PlaylistService} from '../../services/playlist.service';
import {Playlist} from '../../models/playlist.model';

@Component({
  selector: 'app-track-actions',
  templateUrl: './track-actions.component.html',
  styleUrls: ['./track-actions.component.scss']
})
export class TrackActionsComponent implements OnInit {
  @Input() trackId: string;
  playlists: Playlist[];
  constructor(private trackService: TrackService, private playerService: PlayerService, private playlistService: PlaylistService) {
    this.playlists = [];
  }

  ngOnInit() {
    this.playlistService.findAll().subscribe(res => {
      this.playlists = res;
    });
  }
  play(trackId: string) {
    this.trackService.play(trackId).subscribe(() => {
      this.playerService.findTracks();
    });
  }
  enqueue(trackId: string) {
    this.playerService.addTrackToPlayer(trackId).subscribe( () => {
      this.playerService.findTracks();
    });
  }
  addToPlaylistEditor(trackId: string) {
    this.playlistService.addTrackToActiveEditorPlaylist(trackId).subscribe( () => {
      this.playlistService.findEditorTracks();
    });
  }
  addToPlaylist(playlistId: string) {
    this.playlistService.addTrackToPlaylist(this.trackId, playlistId);
  }
}
