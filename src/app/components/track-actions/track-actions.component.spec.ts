import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackActionsComponent } from './track-actions.component';

describe('TrackActionsComponent', () => {
  let component: TrackActionsComponent;
  let fixture: ComponentFixture<TrackActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackActionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
