import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatSort, MatTableDataSource, MatPaginator} from '@angular/material';
import {Track} from '../../models/track.model';
import {TrackService} from '../../services/track.service';
import {CdkVirtualScrollViewport} from '@angular/cdk/scrolling';
import {Playlist} from '../../models/playlist.model';
import {fillViewport} from '../../utility/functions';

@Component({
  selector: 'app-track-page',
  templateUrl: './track-page.component.html',
  styleUrls: ['./track-page.component.scss']
})
export class TrackPageComponent implements OnInit, AfterViewInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  public dataSource: MatTableDataSource<Track>;
  public tracks: Track[];
  displayedColumns = ['actions', 'title', 'artists', 'release', 'duration', 'playCount', 'created'];
  filter = '';
  totalElements = 0;
  scrollIndex = 0;
  loading = true;
  constructor(private trackService: TrackService) { }

  ngOnInit() {
    this.tracks = [];
    this.dataSource = new MatTableDataSource(this.tracks);
    this.dataSource.sort = this.sort;
    this.loadTracks();
  }
  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => {
      this.applyFilter();
    });
  }
  loadTracks(filter = '', sortDirection = 'asc', pageIndex = 0, pageSize = 20, paged = true, append = false) {
    this.loading = true;
    this.trackService.findAllTracks(filter, sortDirection, pageIndex, pageSize, paged, this.sort).subscribe(res => {
      // this.processLoadedTracks(res);
      if (res.page) {
        this.totalElements = res.page.totalElements;
      }
      if (res.length === 0) {
        return;
      }if (append) {
        this.tracks.push(...Track.deserializeArray(res));
      } else {
        this.tracks = Track.deserializeArray(res);
      }
      setTimeout (() => {
        this.dataSource = new MatTableDataSource(this.tracks);
        this.loading = false;
        this.paginator.length = res.page.totalElements;
      }, 1000);
    });
  }
  applyFilter(filter?: string) {
    if (filter === undefined) {
      filter = this.filter;
    }
    this.paginator.firstPage();
    this.loadTracks(filter);
  }
  setFilter(filter: string) {
    this.filter = filter;
    this.applyFilter();
    if (this.paginator) {
      this.paginator.firstPage();
    }
  }
  handleScroll() {
    if (this.paginator.hasNextPage() && !this.loading) {
      this.paginator.nextPage();
      this.loadTracks(this.filter, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize, true, true);
    }
  }

}
