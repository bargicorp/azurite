import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Track} from '../../models/track.model';

@Component({
  selector: 'app-track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.scss']
})
export class TrackComponent implements OnInit {

  @Input() active = false;
  @Input() paused = false;
  @Input() track: Track;
  @Output() play = new EventEmitter();
  @Output() pause = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit() {
  }
  togglePause() {
    this.pause.emit(!this.paused);
  }
  playTrack() {
    this.play.emit();
  }
}
