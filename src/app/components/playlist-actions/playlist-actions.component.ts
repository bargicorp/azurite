import {Component, Input, OnInit} from '@angular/core';
import {Playlist} from '../../models/playlist.model';
import {PlaylistService} from '../../services/playlist.service';
import {PlayerService} from '../../services/player.service';
import {Track} from '../../models/track.model';

@Component({
  selector: 'app-playlist-actions',
  templateUrl: './playlist-actions.component.html',
  styleUrls: ['./playlist-actions.component.scss']
})
export class PlaylistActionsComponent implements OnInit {
  @Input() playlist: Playlist;

  constructor(private playlistService: PlaylistService, private playerService: PlayerService) { }

  ngOnInit() {
  }

  sendToPlaylistEditor() {
    this.playlistService.activeEditorPlaylist = this.playlist;
  }
  play() {
    this.playerService.setTracks(Track.getIds(this.playlist.tracks));
  }
  enqueue() {
    Track.getIds(this.playlist.tracks).forEach(id => {
      this.playerService.addTrackToPlayer(id);
    });
  }


}
