import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {Playlist} from '../../models/playlist.model';
import {PlayerComponent} from '../player/player.component';
import {environment} from '../../../environments/environment';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit, OnDestroy {
  @ViewChild(PlayerComponent) player: PlayerComponent;
  apiAddress = new FormControl(
    environment.apiBaseUri,
      Validators.required
  );
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
  constructor(private breakpointObserver: BreakpointObserver) {}
  ngOnInit() {
    const apiAddress = localStorage.getItem('apiAddress');
    console.log(apiAddress);
    if (apiAddress) {
      this.apiAddress.setValue(apiAddress);
      this.changeApiAddress();
    }
  }
  ngOnDestroy() {
    localStorage.setItem('apiAddress', environment.apiBaseUri);
  }
  public changeApiAddress() {
    environment.apiBaseUri = this.apiAddress.value;
    localStorage.setItem('apiAddress', environment.apiBaseUri);
  }
}
