import {Component, Input, OnInit} from '@angular/core';
import {Artist} from '../../models/artist.model';

@Component({
  selector: 'app-artist-link',
  templateUrl: './artist-link.component.html',
  styleUrls: ['./artist.component.scss']
})
export class ArtistLinkComponent implements OnInit {
  @Input() artists: Artist[];
  constructor() { }

  ngOnInit() {
  }

}
