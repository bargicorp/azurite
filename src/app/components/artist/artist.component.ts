import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Artist} from '../../models/artist.model';
import {IdParser} from '../../utility/id-parser';
import {ReleaseService} from '../../services/release.service';
import {ActivatedRoute} from '@angular/router';
import {Release} from '../../models/release.model';
import {ArtistService} from '../../services/artist.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.scss']
})
export class ArtistComponent implements OnInit {
  loaded: Promise<Boolean>;
  artist: Artist;
  releases: Release[];
  title = 'artist not found';
  constructor(private route: ActivatedRoute,
              private releaseService: ReleaseService,
              private artistService: ArtistService) { }

  ngOnInit() {
    const id = IdParser.parseArtistId(this.route);
    this.artistService.findArtist(id).subscribe(res => {
      this.artist = res;
      this.title = 'Artist: ' + res.name;
      this.artistService.findAllReleases(id).subscribe(releases => {
          this.releases = releases;
          this.loaded = Promise.resolve(true);
        }
      );
    });
  }
}
