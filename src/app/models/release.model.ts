import {Artist} from './artist.model';
import {Track} from './track.model';
import {ResourceObject} from './resource-object.model';

export class Release extends ResourceObject {
  id: string;
  title: string;
  coverArt: string;
  artists: Artist[];
  tracks: Track[];
  _links: any;

  constructor(id: string = null, title: string = '', coverUri = null, artists: Artist[] = [],
              links: any, createdDate?: string, lastModifiedDate?: string, ) {
    super(createdDate, lastModifiedDate, links);
    this.id = id;
    this.title = title;
    if (coverUri) {
      this.coverArt = coverUri;
    } else {
      this.coverArt = './assets/no-cover.png';
    }
    this.artists = artists;
    this._links = links;
  }
  static deserialize(json: any): Release {
    const artists = [];
    if (json._embedded) {
      if (json._embedded.artists) {
        artists.push(json._embedded.artists);
      } else if (json._embedded.artist) {
        artists.push(json._embedded.artist);
      }
    }
    return new Release(
      json.id,
      json.name,
      json.coverArt,
      artists,
      json._links,
      json.createdDate,
      json.lastModifiedDate,
    );
  }
  static deserializeArray(json: any): Release[] {
    const releases = [];
    if (json._embedded) {
      if (json._embedded.releases) {
        json._embedded.releases.forEach((release) => {
          releases.push(Release.deserialize(release));
        });
      } else {
        json.releases.forEach((release) => {
          releases.push(Release.deserialize(release));
        });
      }
    } else {
      json.forEach((release) => {
        releases.push(Release.deserialize(release));
      });
    }
    return releases;
  }
}
