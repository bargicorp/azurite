import {Artist} from './artist.model';
import {Release} from './release.model';
import {ResourceObject} from './resource-object.model';

export class Track extends ResourceObject {
  id: string;
  title: string;
  private _duration: number;
  playCount: number;
  private _artists: Artist[];
  release: Release;
  constructor(id: string = null, title: string = '', duration = null, playCount = null,
              createdDate?: string, lastModifiedDate?: string, links = {}) {
    super(createdDate, lastModifiedDate, links);
    this.id = id;
    this.title = title;
    this._duration = duration;
    this.playCount = playCount;
  }
  static getIds(tracks: Track[]): string[] {
    const trackIds: string[] = [];
    tracks.forEach(track => {
      trackIds.push(track.id);
    });
    return trackIds;
  }
  static deserialize(json: any): Track {
    let links;
    if (json._links) {
      links = {
        self: json._links.self && json._links.self.href || ''
      };
    }
    const artists = [];
    if (json._embedded.artists) {
      artists.push(json._embedded.artists);
    } else if (json._embedded.artist) {
      artists.push(json._embedded.artist);
    }
    const track = new Track(
      json.id,
      json.title,
      json.duration,
      json.playCount,
      json.createdDate,
      json.lastModifiedDate,
      links
    );
    track.artists = artists;
    if (json._embedded.release) {
      track.release = Release.deserialize(json._embedded.release);
    }
    return(track);
  }
  static deserializeArray(json: any): Track[] {
    const tracks = [];
    if (json._embedded) {
      if (json._embedded.tracks) {
        json._embedded.tracks.forEach((track) => {
          tracks.push(Track.deserialize(track));
        });
      } else {
        json.tracks.forEach((track) => {
          tracks.push(Track.deserialize(track));
        });
      }
    } else {
      json.forEach((track) => {
        tracks.push(Track.deserialize(track));
      });
    }
    return tracks;
  }
  get duration(): string {
    let h, m, s: number;
    h = Math.floor(this._duration / 3600);
    m = Math.floor((this._duration - (3600 * h)) / 60);
    s = this._duration - (3600 * h) - (60 * m);
    let minutes, seconds, result = '';
    minutes = m;
    if (h > 0) {
      result = h + ':';
      if (m < 10) {
        minutes = '0' + m;
      }
    }
    if (s < 10) {
      seconds = '0' + s;
    } else {
      seconds = s;
    }
    result += minutes + ':' + seconds;
    return result;
  }

  set duration(value: string) {
    const regex = new RegExp('\d{0,2}:\d{0,2}:\d{1,2}');
    if (regex.test(value)) {
      const d = value.split(':', 2);
      let duration = 0;
      let i: number;
      for (i = d.length; i > 0; i--) {
        duration += parseInt(d[i - 1], 10) * (60 ** (d.length - i));
      }
      this._duration = duration;
    }
  }

  set artists(value: Artist[]) {
    this._artists = value;
  }
  get artists(): Artist[] {
    return this._artists;
  }
}
