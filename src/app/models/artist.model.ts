import {ResourceObject} from './resource-object.model';

export class Artist extends ResourceObject {
  id: string;
  name: string;
  trackCount: number;

  constructor(id?: string, name?: string, trackCount?: number,
              createdDate?: string, lastModifiedDate?: string,
              links = {self: '', tracks: ''}) {
    super(createdDate, lastModifiedDate, links);
    this.id = id;
    this.name = name;
    this.trackCount = trackCount;
  }
  static deserialize(json: any): Artist {
    let links;
    if (json._links) {
      links = {
        self: json._links.self && json._links.self.href || '',
        tracks: json._links.tracks && json._links.tracks.href || ''
      };
    } else if (json.links) {
      links = {
        self: json.links.self && json.links.self.href || '',
        tracks: json.links.tracks && json.links.tracks.href || ''
      };
    }
    return new Artist(
      json.id,
      json.name,
      json.trackCount,
      json.createdDate,
      json.lastModifiedDate,
      links
    );
  }
  static deserializeArray(json: any): Artist[] {
    const artists = [];
    if (json._embedded) {
      if (json._embedded.artists) {
        json._embedded.artists.forEach((artist) => {
          artists.push(Artist.deserialize(artist));
        });
      } else {
        json.artists.forEach((artist) => {
          artists.push(Artist.deserialize(artist));
        });
      }
    } else {
      json.forEach((artist) => {
        artists.push(Artist.deserialize(artist));
      });
    }
    return artists;
  }
}
