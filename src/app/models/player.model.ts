import {Track} from './track.model';

export enum PlayerAction {
  STOP, PLAY, PAUSE, SEEK, PREVIOUS, NEXT
}
export class Player {
  id: string;
  volume: number;
  index: number;
  time: number;
  status: string;
  paused: boolean;
  tracks: Track[];

  constructor(
    id: string = null,
    volume: number = null,
    index: number = null,
    time: number = null,
    paused: string = null,
    tracks: Track[] = []) {
    this.id = id;
    this.volume = volume;
    this.index = index;
    this.time = time;
    this.status = status;
    this.tracks = tracks;
  }
}
