export class ResourceObject {
  private _createdDate: Date;
  private _lastModifiedDate: Date;
  _links: any;
  constructor(createdDate?: string, lastModifiedDate?: string, links?: any) {
    if (createdDate) {
      this._createdDate = new Date(createdDate);
    }
    if (lastModifiedDate) {
      this._lastModifiedDate = new Date(lastModifiedDate);
    }
    this._links = links;
  }
  get createdDate(): String {
    if (this._createdDate) {
      return this._createdDate.toLocaleDateString();
    } else {
      return '';
    }
  }
  get lastModifiedDate(): String {
    if (this._lastModifiedDate) {
      return this._lastModifiedDate.toLocaleDateString();
    } else {
      return '';
    }
  }
}
