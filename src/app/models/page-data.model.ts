export class PageData {
  number: number;
  size: number;
  totalElements: number;
  totalPages: number;
  constructor(number = 0, size = 50, totalElements?: number, totalPages?: number) {
    this.number = number;
    this.size = size;
    this.totalElements = totalElements;
    this.totalPages = totalPages;
  }
  static deserialize(json: any): PageData {
    return new PageData(
      json.number,
      json.size,
      json.totalElements,
      json.totalPages
    );
  }
}
