import {Track} from './track.model';
import {ResourceObject} from './resource-object.model';

export class Playlist extends ResourceObject {
  id: string;
  name: string;
  tracks: Track[];

  constructor(id?: string, name?: string, createdDate?: string, lastModifiedDate?: string,  links = {self: '', tracks: ''}) {
    super(createdDate, lastModifiedDate, links);
    this.id = id;
    this.name = name;
  }
  static deserialize(json: any): Playlist {
    let links;
    if (json._links) {
        links = {
          self: json._links.self && json._links.self.href || '',
          tracks: json._links.tracks && json._links.tracks.href || ''
      };
    }
    return new Playlist(
      json.id,
      json.name,
      json.createdDate,
      json.lastModifiedDate,
      links
    );
  }
  static deserializeArray(json: any): Playlist[] {
    const playlists = [];
    if (json._embedded && json._embedded.playlists) {
      json._embedded.playlists.forEach((track) => {
        playlists.push(Playlist.deserialize(track));
      });
    }
    return playlists;
  }
}
