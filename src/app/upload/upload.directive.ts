import {Directive, EventEmitter, HostBinding, HostListener, Output} from '@angular/core';

@Directive({
  selector: '[appUpload]'
})
export class UploadDirective {
  @Output() files = new EventEmitter<FileList>();
  @HostBinding('style.background') private background = '#424242';
  @HostBinding('style.border-color') private border = '#666666';

  @HostListener('drop', ['$event'])
  onDrop($event) {
    $event.preventDefault();
    this.resetStyle();
    this.files.emit($event.dataTransfer.files);
  }
  @HostListener('dragover', ['$event'])
  onDragOver($event) {
    $event.preventDefault();
    $event.stopPropagation();
    this.background = '#555555';
    this.border = '#666666';
  }
  @HostListener('dragleave', ['$event'])
  onDragLeave($event) {
    $event.preventDefault();
    $event.stopPropagation();
    this.resetStyle();
  }
  private resetStyle() {
    this.background = '#424242';
    this.border = '#666666';
  }
}
