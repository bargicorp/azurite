import {ActivatedRoute, UrlSegment} from '@angular/router';
import {UriSegments} from '../services/uri-segments';

export class IdParser {

  private static parseId(route: ActivatedRoute, precedentSegment: string): string {
    const segments: UrlSegment[] = route.snapshot.url;
    for (let i = 0; i < segments.length; i++) {
      if (segments[i].path === precedentSegment && i < (segments.length - 1)) {
        return segments[i + 1].path;
      }
    }
    return null;
  }
  public static parseReleaseId(route: ActivatedRoute): string {
    return IdParser.parseId(route, 'releases');
  }
  public static parseTrackId(route: ActivatedRoute): string {
    return IdParser.parseId(route, 'tracks');
  }
  public static parseArtistId(route: ActivatedRoute): string {
    return IdParser.parseId(route, 'artists');
  }
}
