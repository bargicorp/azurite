export class Regex {
  public static readonly uri = '/(^|\s)((https?:\/\/)?[\w-]+(.[\w-]+)+.?(:?\d+)?(\/\S*)?)/gi';
}
