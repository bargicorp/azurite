import {ElementRef} from '@angular/core';
import {environment} from '../../environments/environment';

export function fillViewport(elementId: string) {
  if (ElementRef) {
    const h = window.innerHeight - document.getElementById(elementId).getBoundingClientRect().top;
    document.getElementById(elementId).style.setProperty('height', h + 'px');
  }
}
export function segmentsToUriString(segments: string[], baseUri = environment.apiBaseUri): string {
  let uri: string;
  if (baseUri) {
    uri = baseUri;
  } else {
    uri = 'localhost:8080';
  }
  for (const segment of segments) {
    uri += '/';
    uri += segment;
  }
  return uri;
}
