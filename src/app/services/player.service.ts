import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {map} from 'rxjs/operators';
import {Player} from '../models/player.model';
import {TrackService} from './track.service';
import {environment} from '../../environments/environment';
import {UriSegments} from './uri-segments';

@Injectable()
export class PlayerService {
  private trackDataSource = new Subject<any[]>();
  private playerDataSource = new Subject<any>();
  private playlistEditorTitleSource = new Subject<String>();
  public trackData = this.trackDataSource.asObservable();
  public playerData = this.playerDataSource.asObservable();
  public playlistEditorTitle = this.playlistEditorTitleSource.asObservable();
  webSocketIgnoreBuffer = 0;
  webSocketIgnoreBufferTimeout;
  constructor(public http: HttpClient) {
  }
  findPlayer() {
    this.http.get<any>(environment.apiBaseUri + UriSegments.player).pipe(
      map((res: any) => {
        this.playerDataSource.next(res);
      })).subscribe();
  }
  findTracks() {
    this.http.get<any>(environment.apiBaseUri + UriSegments.player + UriSegments.track).pipe(
      map((res: any) => {
        this.trackDataSource.next(res);
      })).subscribe();
  }
  addTrackToPlayer(trackId: string, index?: number): Observable<any> {
    let dto;
    if (index) {
      dto = {
      id: trackId,
      trackNo: index
      };
    } else {
      dto = {
        id: trackId
      };
    }
    return this.http.post<any>(environment.apiBaseUri + UriSegments.player + UriSegments.track, dto,
      {headers: new HttpHeaders({'Content-Type': 'application/json'})});
  }
  moveTrack(previousIndex: number, currentIndex: number): Observable<any> {
    const dto = {
      trackNo: currentIndex
    };
    return this.http.patch<any>(environment.apiBaseUri + UriSegments.player + UriSegments.track + '/' + previousIndex, dto,
      {headers: new HttpHeaders({'Content-Type': 'application/json'})});
  }
  stop() {
    const dto = {
      id: 0,
      time: 0,
      paused: true,
    };
    this.http.patch<any>(environment.apiBaseUri + UriSegments.player, dto,
      {headers: new HttpHeaders({'Content-Type': 'application/json'})}).subscribe();
  }
  play(index: number) {
    const dto = {
      id: 0,
      index: index,
      paused: false
    };
    this.http.patch<any>(environment.apiBaseUri + UriSegments.player, dto,
      {headers: new HttpHeaders({'Content-Type': 'application/json'})}).subscribe();
  }
  togglePause(paused: boolean) {
    const dto = {
      id: 0,
      paused: !paused,
    };
    this.http.patch<any>(environment.apiBaseUri + UriSegments.player, dto,
      {headers: new HttpHeaders({'Content-Type': 'application/json'})}).subscribe();
  }
  setTracks(trackIds: string[], index: number = 0) {
    const dto = {
      id: 0,
      index: index,
      trackIds: trackIds,
      paused: false
    };
    return this.http.patch<any>(environment.apiBaseUri + UriSegments.player, dto,
      {headers: new HttpHeaders({'Content-Type': 'application/json'})}).subscribe();
  }
  incrementWebsocketIgnoreBuffer() {
    if (this.webSocketIgnoreBufferTimeout) {
      window.clearTimeout(this.webSocketIgnoreBufferTimeout);
    }
    this.webSocketIgnoreBuffer++;
    this.webSocketIgnoreBufferTimeout = window.setTimeout(() => {this.webSocketIgnoreBuffer = 0; }, 5000);
  }
  expireWebsocketIgnoreBuffer(): boolean {
    if (this.webSocketIgnoreBuffer > 0) {
      this.webSocketIgnoreBuffer--;
      return false;
    }
    return true;
  }
}
