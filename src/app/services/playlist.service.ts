import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of, Subject} from 'rxjs';
import {Playlist} from '../models/playlist.model';
import {Track} from '../models/track.model';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {UriSegments} from './uri-segments';
import {jsonHeader} from './json-header';

@Injectable()
export class PlaylistService {
  private _activeEditorPlaylist: Playlist = null;
  private editorTrackDataSource = new Subject<Track[]>();
  public editorTrackData = this.editorTrackDataSource.asObservable();
  private editorTitleSubject = new Subject<string>();
  public editorTitle = this.editorTitleSubject.asObservable();
  constructor(public http: HttpClient) {
  }
  set activeEditorPlaylist(playlist: Playlist) {
    this.editorTitleSubject.next(playlist.name);
    this._activeEditorPlaylist = playlist;
    this.findEditorTracks();
  }
  get activeEditorPlaylist() {
    return this._activeEditorPlaylist;
  }
  findEditorTracks() {
    this.http.get<any>(environment.apiBaseUri + UriSegments.playlist + '/'
      + this._activeEditorPlaylist.id + UriSegments.track).pipe(
      map((res: any) => {
        this.editorTrackDataSource.next(Track.deserializeArray(res));
      })).subscribe();
  }
  findAll(): Observable<Playlist[]>  {
    return this.http.get(environment.apiBaseUri + UriSegments.playlist).pipe(
      map(res => {
        return Playlist.deserializeArray(res);
      })
    );
  }
  create(): Observable<Playlist> {
    return this.http.post<any>(environment.apiBaseUri + UriSegments.playlist, {},
      {headers: new HttpHeaders({'Content-Type': 'application/json'})});
  }
  findTracks(): Observable<Track[]> {
    return this.http.get<any>(environment.apiBaseUri + UriSegments.playlist).pipe(
      map(res => {
          return Track.deserializeArray(res);
      })
    );
  }
  moveTrack(playlist: Playlist, previousIndex: number, currentIndex: number): Observable<any> {
    const dto = {
      trackNo: currentIndex
    };
    return this.http.patch<any>(environment.apiBaseUri
      + UriSegments.playlist
      + '/' + playlist.id + UriSegments.track
      + '/' + previousIndex, dto,
      {headers: new HttpHeaders({'Content-Type': 'application/json'})});
  }
  addTrackToActiveEditorPlaylist(trackId: string, index?: number): Observable<any> {
    let dto;
    if (index) {
      dto = {
        id: trackId,
        trackNo: index
      };
    } else {
      dto = {
        id: trackId
      };
    }

    console.log(dto);
    return this.http.post<any>(environment.apiBaseUri + UriSegments.playlist
      + '/' + this._activeEditorPlaylist.id
      + UriSegments.track, dto,
      {headers: new HttpHeaders({'Content-Type': 'application/json'})});
  }
  addTrackToPlaylist(trackId: string, playlistId: string, index?: number) {
    let dto;
    if (index) {
      dto = {
        id: trackId,
        trackNo: index
      };
    } else {
      dto = {
        id: trackId
      };
    }
    console.log(dto);
    this.http.post<any>(environment.apiBaseUri + UriSegments.playlist
      + '/' + playlistId
      + UriSegments.track, dto,
      {headers: new HttpHeaders({'Content-Type': 'application/json'})}).subscribe();
  }
  loadTracks(playlist: Playlist): Observable<Playlist> {
    return this.http.get(playlist._links.tracks).pipe(
      map(res => {
        playlist.tracks = Track.deserializeArray(res);
        return playlist;
    }));
  }
  update(playlist: Playlist): Observable<Playlist> {
    const dto = {
      name: playlist.name
    };
    return this.http.patch<Playlist>(playlist._links.self, dto, jsonHeader).pipe(
      map(res => {
        return Playlist.deserialize(res);
      })
    );
  }
  delete(playlist): Observable<any> {
    return this.http.delete(playlist._links.self);
  }
}
