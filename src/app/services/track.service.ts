import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Track} from '../models/track.model';
import {map} from 'rxjs/operators';
import {MatSort} from '@angular/material';
import {environment} from '../../environments/environment';
import {UriSegments} from './uri-segments';

@Injectable()
export class TrackService {
  constructor(public http: HttpClient) {
  }
  findAll(): Observable<Track[]> {
    // http request
    return this.http.get<any>(environment.apiBaseUri + UriSegments.track).pipe(
      map(res => res._embedded.tracks.map((i) => {
        return new Track(
          i.id,
          i.title,
          i.duration,
          i.playCount,
          i.created
        );
      })
    ));
  }
  findAllTracks(query = '', sortOrder = 'asc',
                pageNumber = 0, pageSize = 0, paged = true, sort: MatSort = null): Observable<any> {
    let httpParams = new HttpParams()
      .set('query', query)
      .set('page', pageNumber.toString())
      .set('size', pageSize.toString())
      .set('paged', paged.toString());
    if (sort && sort.active !== undefined) {
      httpParams = httpParams.set('sort', sort.active + ',' + sort.direction.toString());
    }
    return this.http.get<any>(environment.apiBaseUri + UriSegments.track, {
      params: httpParams
    }).pipe(
      map((res: any) => {
        return res;
      })
    );
  }
  findAllTracksOfPlaylist(playlistId: string): Observable<any> {
    return this.http.get<any>(environment.apiBaseUri + UriSegments.playlist + '/' + playlistId + UriSegments.track);
  }
  play(trackId: string): Observable<any> {
    const dto = {
      id: 0,
      index: 0,
      trackIds: [trackId],
      paused: false
    };
    return this.http.patch<any>(environment.apiBaseUri + UriSegments.player,
      dto, {headers: new HttpHeaders({'Content-Type': 'application/json'})});
  }
  findRandom(number: number): Observable<Track[]> {
    return this.http.get<any>(environment.apiBaseUri + UriSegments.track + UriSegments.random + '/' + number).pipe(
      map(res => {
        return Track.deserializeArray(res);
      })
    );
  }
}
