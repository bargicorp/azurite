import {HttpHeaders} from '@angular/common/http';

export const jsonHeader = {headers: new HttpHeaders({'Content-Type': 'application/json'})};
