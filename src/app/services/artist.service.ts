import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Artist} from '../models/artist.model';
import {environment} from '../../environments/environment';
import {UriSegments} from './uri-segments';
import {MatSort} from '@angular/material';
import {Track} from '../models/track.model';
import {Release} from '../models/release.model';

@Injectable()
export class ArtistService {
  constructor(public http: HttpClient) {
  }
  findAll(query = '', sortOrder = 'asc',
          pageNumber = 0, pageSize = 0, paged = true, sort: MatSort = null): Observable<any> {
    let httpParams = new HttpParams()
      .set('query', query)
      .set('page', pageNumber.toString())
      .set('size', pageSize.toString())
      .set('paged', paged.toString());
    if (sort && sort.active !== undefined) {
      httpParams = httpParams.set('sort', sort.active + ',' + sort.direction.toString());
    }
    return this.http.get<any>(environment.apiBaseUri + UriSegments.artist, {params: httpParams}).pipe(
      map(res =>  {
          return res;
        })
    );
  }
  findArtist(id: String): Observable<Artist> {
    return this.http.get<any>(environment.apiBaseUri + UriSegments.artist + '/' + id).pipe(
      map(res => {
        return Artist.deserialize(res);
      })
    );
  }
  findRandom(number: number): Observable<Artist[]> {
    return this.http.get<any>(environment.apiBaseUri + UriSegments.artist + UriSegments.random + '/' + number).pipe(
      map(res => {
        return Artist.deserializeArray(res);
      })
    );
  }
  findAllReleases(id: String): Observable<Release[]> {
    return this.http.get<any>(environment.apiBaseUri + UriSegments.artist + '/' + id + UriSegments.release).pipe(
      map(res =>  {
        return Release.deserializeArray(res);
      })
    );
  }
}
