import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {UriSegments} from './uri-segments';

@Injectable()
export class FileService {
  constructor(public http: HttpClient) {
  }
  upload(file: File) {
    const formData = new FormData();
    formData.append('file', file);
    this.http.post<any>(
      environment.apiBaseUri + UriSegments.file,
      formData,
    ).subscribe(res => {
    })
    ;
  }
}
