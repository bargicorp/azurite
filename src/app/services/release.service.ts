import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {Release} from '../models/release.model';
import {MatSort} from '@angular/material';
import {Track} from '../models/track.model';
import {UriSegments} from './uri-segments';

@Injectable()
export class ReleaseService {
  constructor(protected http: HttpClient) {}
  findAll(query = '', sortOrder = 'asc',
          pageNumber = 0, pageSize = 0, paged = true, sort: MatSort = null): Observable<any> {
    let httpParams = new HttpParams()
      .set('query', query)
      .set('page', pageNumber.toString())
      .set('size', pageSize.toString())
      .set('paged', paged.toString());
    if (sort && sort.active !== undefined) {
      httpParams = httpParams.set('sort', sort.active + ',' + sort.direction.toString());
    }
    return this.http.get<any>(environment.apiBaseUri + UriSegments.release,
      {params: httpParams}).pipe(
      map((res: any) => {
          return res;
        })
    );
  }
  findRelease(id: String): Observable<Release> {
    return this.http.get<any>(environment.apiBaseUri + UriSegments.release + '/' + id).pipe(
      map(res => {
        return Release.deserialize(res);
      })
    );
  }
  getTracks(releaseId: string): Observable<Track[]> {
    return this.http.get<any>(environment.apiBaseUri + UriSegments.release + '/' + releaseId + UriSegments.track).pipe(
      map( res => {
        return res;
      })
    );
  }
  findCover(release: Release): Observable<string> {
      return this.http.get<any>(release._links.coverArt.href).pipe(
        map((res: any) => {
          return res.base64encodedBinary;
      })
    );
  }
  findRandom(number: number): Observable<Release[]> {
    return this.http.get<any>(environment.apiBaseUri + UriSegments.release + UriSegments.random + '/' + number).pipe(
      map(res => {
        return Release.deserializeArray(res);
      })
    );
  }
}
