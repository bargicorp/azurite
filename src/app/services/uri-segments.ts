export class UriSegments {
  public static readonly artist = '/artists';
  public static readonly file = '/files';
  public static readonly track = '/tracks';
  public static readonly release = '/releases';
  public static readonly playlist = '/playlists';
  public static readonly player = '/player';
  public static readonly random = '/random';
}
