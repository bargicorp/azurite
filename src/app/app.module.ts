import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import {
  MatGridListModule,
  MatCardModule,
  MatIconModule,
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatListModule,
  MatTableModule,
  MatSortModule,
  MatFormFieldModule,
  MatInputModule,
  MatTabsModule,
  MatTooltipModule,
  MatProgressBarModule, MatButtonToggleModule, MatExpansionModule
} from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import {RouterModule, Routes} from '@angular/router';
import {TrackService} from './services/track.service';
import {HttpClientModule} from '@angular/common/http';
import {ArtistService} from './services/artist.service';
import {UploadDirective} from './upload/upload.directive';
import {FileService} from './services/file.service';
import {ReleaseService} from './services/release.service';
import { ReleaseCardComponent } from './components/release-card/release-card.component';
import { NavComponent } from './components/nav/nav.component';
import { PlaylistComponent } from './components/playlist/playlist.component';
import { PlayerComponent } from './components/player/player.component';
import {PlayerService} from './services/player.service';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { TrackComponent } from './components/track/track.component';
import {InjectableRxStompConfig, RxStompService, rxStompServiceFactory} from '@stomp/ng2-stompjs';
import {rxStompConfig} from './rx-stomp.config';
import { TrackPageComponent } from './components/track-page/track-page.component';
import { CollectionContainerComponent } from './components/collection-container/collection-container.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {ScrollingModule} from '@angular/cdk/scrolling';
import { ArtistComponent } from './components/artist/artist.component';
import {ArtistLinkComponent} from './components/artist/artist-link.component';
import { ReleasePageComponent } from './components/release-page/release-page.component';
import { ReleaseComponent } from './components/release/release.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {PlaylistPageComponent} from './components/playlist-page/playlist-page.component';
import {PlaylistService} from './services/playlist.service';
import {MatMenuModule} from '@angular/material/menu';
import { TrackActionsComponent } from './components/track-actions/track-actions.component';
import { ReleaseActionsComponent } from './components/release-actions/release-actions.component';
import { PlaylistActionsComponent } from './components/playlist-actions/playlist-actions.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { UploadComponent } from './components/upload/upload.component';
import {ObserversModule} from '@angular/cdk/observers';
import { ArtistPageComponent } from './components/artist-page/artist-page.component';

const routes: Routes = [
  { path: '', component: DashboardComponent},
  { path: 'artists', component: ArtistPageComponent},
  { path: 'artists/:id', component: ArtistComponent},
  { path: 'tracks', component: TrackPageComponent},
  { path: 'releases', component: ReleasePageComponent},
  { path: 'releases/:id', component: ReleaseComponent},
  { path: 'playlists', component: PlaylistPageComponent}
];

@NgModule({
  declarations: [
    UploadDirective,
    AppComponent,
    DashboardComponent,
    ReleaseCardComponent,
    NavComponent,
    PlaylistComponent,
    PlayerComponent,
    TrackComponent,
    TrackPageComponent,
    CollectionContainerComponent,
    ArtistComponent,
    ArtistLinkComponent,
    ReleasePageComponent,
    ReleaseComponent,
    PlaylistPageComponent,
    TrackActionsComponent,
    ReleaseActionsComponent,
    PlaylistActionsComponent,
    UploadComponent,
    ArtistPageComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    DragDropModule,
    FlexLayoutModule,
    HttpClientModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatTabsModule,
    MatButtonModule,
    LayoutModule,
    RouterModule.forRoot(routes, {useHash: false}),
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatTooltipModule,
    MatProgressBarModule,
    MatButtonToggleModule,
    ScrollingModule,
    FormsModule,
    MatExpansionModule,
    ObserversModule,
    ReactiveFormsModule,

],
  providers: [
    ArtistService,
    TrackService,
    FileService,
    ReleaseService,
    PlayerService,
    PlaylistService,
    {
      provide: InjectableRxStompConfig,
      useValue: rxStompConfig
    },
    {
      provide: RxStompService,
      useFactory: rxStompServiceFactory,
      deps: [InjectableRxStompConfig]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
